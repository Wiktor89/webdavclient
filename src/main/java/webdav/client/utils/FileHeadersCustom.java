package webdav.client.utils;

public abstract class FileHeadersCustom {

  public static final String IIN = "iin";

  public static final String FILE_NAME = "file_name";

  public static final String DATE = "date";

  public static final String NUMBER_REQUEST = "number_request";

  public static final String SEPARATOR = "/";
}
