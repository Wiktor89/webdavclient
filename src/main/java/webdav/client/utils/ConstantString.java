package webdav.client.utils;

public abstract class ConstantString {

  public static final String BASIC_AUTHORIZATION = "Basic Authorization";
  public static final String FTP = "webdav";
}
