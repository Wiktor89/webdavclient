package webdav.client.utils;

public abstract class HttpMethodWebDav {

  /**
   *  Получение свойств объекта на сервере в формате XML
   */
  public static final String PROPFIND = "PROPFIND";

  /**
   * Изменение свойств объекта
   */
  public static final String PROPPATCH = "PROPPATCH";

  /**
   * Создать папку на сервере
   */
  public static  final String MKCOL = "MKCOL";

  /**
   * Копирование на стороне сервера
   */
  public static final String COPY = "COPY";

  /**
   *  Перемещение на стороне сервера
   */
  public static final String MOVE = "MOVE";

  /**
   * Заблокировать объект
   */
  public static final String LOCK = "LOCK";

  /**
   * Раблокировать объект
   */
  public static final String UNLOCK= "UNLOCK";
}
