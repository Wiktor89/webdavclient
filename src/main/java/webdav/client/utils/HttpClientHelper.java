package webdav.client.utils;

import webdav.client.config.WebDavServerConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.NTCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHttpRequest;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.stereotype.Component;

import javax.net.ssl.SSLContext;
import java.io.InputStream;
import java.net.InetAddress;
import java.util.stream.Stream;

import static webdav.client.utils.FileHeadersCustom.SEPARATOR;
import static webdav.client.utils.HttpMethodWebDav.MKCOL;
import static java.nio.charset.StandardCharsets.UTF_8;

@Component
@Slf4j
@RequiredArgsConstructor
public class HttpClientHelper {

  private final WebDavServerConfig webDavServerConfig;

  public void createDir(String urlDir) {
    log.debug("createDir HOST {} ", webDavServerConfig.getUrl());
    log.debug("createDir HOST PORT {} ", webDavServerConfig.getPort());
    log.debug("createDir HOST PROT {} ", webDavServerConfig.getProt());
    final String[] pathDir = urlDir.split(SEPARATOR);
    StringBuilder rootDir = new StringBuilder(webDavServerConfig.getHostRootDir());
    Stream.of(pathDir).forEachOrdered(val -> {
      rootDir.append(SEPARATOR).append(val);
      HttpHost host = new HttpHost(webDavServerConfig.getUrl(), webDavServerConfig.getPort(), webDavServerConfig.getProt());
      log.debug("Create dir {} ", rootDir);
      BasicHttpRequest mkcol = new BasicHttpRequest(MKCOL, rootDir.toString());
      try {
        final String hostName = InetAddress.getLocalHost().getHostName();
        log.debug("HostName {} ", hostName);
        SSLContext sslContext = new SSLContextBuilder()
            .loadTrustMaterial(null, (certificate, authType) -> true).build();
        CloseableHttpClient httpClient = HttpClients.custom()
            .setSSLContext(sslContext)
            .setSSLHostnameVerifier(new NoopHostnameVerifier())
            .build();

        CredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(AuthScope.ANY, new NTCredentials(webDavServerConfig.getLogin(), webDavServerConfig.getPassword(), hostName, webDavServerConfig.getDomain()));
        HttpClientContext context = HttpClientContext.create();
        context.setCredentialsProvider(credsProvider);
        HttpGet get = new HttpGet(webDavServerConfig.getHostRootDir());
        final CloseableHttpResponse getResponse = httpClient.execute(host, get, context);
        print(getResponse.getEntity().getContent());
        final CloseableHttpResponse mkolResponse = httpClient.execute(host, mkcol, context);
        print(mkolResponse.getEntity().getContent());
      } catch (Exception e) {
        log.debug("Error creating directory rootDir {} ", rootDir, e);
      }
    });
  }

  private void print(InputStream stream) {
    try {
      final String result = IOUtils.toString(stream, UTF_8);
      log.debug("Response create dir {} ", result);
    } catch (Exception e) {
      log.error("Error {} ",e);
    }
  }
}
