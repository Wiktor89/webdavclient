package webdav.client.controller;

import webdav.client.service.MessageSendService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Api(value = "DocRepController: контроллер для работы с 'Репозиторием документов'")
@RestController
@Slf4j
@RequiredArgsConstructor
public class DocRepController {

  private final MessageSendService service;

  @RequestMapping(method = RequestMethod.OPTIONS)
  public ResponseEntity<?> optionsAPI() {
    return ResponseEntity.ok().allow(HttpMethod.POST).build();
  }

  @ApiOperation(value = "Загрузка файла", notes = "Загрузка файла")
  @PostMapping("/uploadFile")
  public String uploadFile(@RequestParam(value = "file") MultipartFile file, @RequestParam(value = "param") String param) {
    return service.send(file, param);
  }
}