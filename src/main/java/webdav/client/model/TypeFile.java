package webdav.client.model;

public enum TypeFile {

  PROFILE("profile");

  private final String type;

  TypeFile(String type) {
    this.type = type;
  }

  @Override
  public String toString() {
    return type;
  }
}
