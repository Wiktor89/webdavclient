package webdav.client.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class ConsumerDetails implements Serializable {

  private String iin;

  private String numberTask;

  private TypeFile fileType;

  private TypeDir dirType;
}
