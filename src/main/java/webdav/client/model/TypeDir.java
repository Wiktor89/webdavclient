package webdav.client.model;

public enum TypeDir {

  TYPE_DOCUMENTS_ONE("type_documents_one"),
  TYPE_DOCUMENTS_TWO("type_documents_two"),
  TYPE_DOCUMENTS_THREE("type_documents_three");

  private final String typeDir;

  TypeDir(String val) {
    this.typeDir = val;
  }

  @Override
  public String toString() {
    return typeDir;
  }
}
