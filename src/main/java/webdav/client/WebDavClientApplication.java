package webdav.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.integration.config.EnableIntegration;

@SpringBootApplication
@EnableIntegration
@EnableConfigurationProperties
@Slf4j
public class WebDavClientApplication {

  public static void main(String[] args) {
    SpringApplication.run(WebDavClientApplication.class, args);
  }
}