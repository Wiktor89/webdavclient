package webdav.client.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "webdav")
@Component
@Data
public class WebDavServerConfig {

  private String login;

  private String password;

  private String hostRootDir;

  private String url;

  private Integer port;

  private String prot;

  private String domain;
}
