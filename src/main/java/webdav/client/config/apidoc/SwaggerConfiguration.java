package webdav.client.config.apidoc;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StopWatch;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import static webdav.client.utils.ConstantString.BASIC_AUTHORIZATION;
import static webdav.client.utils.ConstantString.FTP;

@Configuration
@EnableSwagger2
@Slf4j
public class SwaggerConfiguration {

  @Value("${appBaseName}")
  private String appBasePath;

  public static final Contact DEFAULT_CONTACT = new Contact("app", "app", "afonin.vitia2000@yandex.ru");

  public static final ApiInfo DEFAULT_API_INFO = new ApiInfo(
      "Api", "Апи", "1.0",
      "urn:tos", DEFAULT_CONTACT,
      "GNU AGPLv3", "https://www.gnu.org/licenses/agpl-3.0.ru.html", Collections.emptyList());

  private static final Set<String> DEFAULT_PRODUCES_AND_CONSUMES = new HashSet<>(Collections.singletonList("application/json"));


  @Bean
  public Docket api() {
    log.debug("Starting Swagger Api Doc");
    StopWatch watch = new StopWatch();
    watch.start();
    Docket docket = new Docket(DocumentationType.SWAGGER_2)
        .apiInfo(DEFAULT_API_INFO)
        .produces(DEFAULT_PRODUCES_AND_CONSUMES)
        .consumes(DEFAULT_PRODUCES_AND_CONSUMES)
        .forCodeGeneration(true)
        .securitySchemes(Collections.singletonList(new BasicAuth(BASIC_AUTHORIZATION)))
        .securityContexts(Collections.singletonList(SecurityContext.builder()
            .securityReferences(
                Collections.singletonList(SecurityReference.builder()
                    .reference(BASIC_AUTHORIZATION)
                    .scopes(new AuthorizationScope[]{new AuthorizationScope("", "Every call to the API is secured with a basic authorization token.")})
                    .build())).build()))
        .genericModelSubstitutes(ResponseEntity.class, Optional.class, CompletableFuture.class)
        .ignoredParameterTypes(java.sql.Date.class)
        .directModelSubstitute(java.time.LocalDate.class, java.sql.Date.class)
        .directModelSubstitute(java.time.ZonedDateTime.class, Date.class)
        .directModelSubstitute(java.time.LocalDateTime.class, Date.class)
        .select()
        .apis(RequestHandlerSelectors.basePackage(FTP))
        .paths(PathSelectors.any())
        .build();
    if (appBasePath != null && !appBasePath.isEmpty()) {
      docket.pathMapping(appBasePath);
    }
    watch.stop();
    log.debug("Started Swagger in {} ms", watch.getTotalTimeMillis());
    return docket;
  }
}
