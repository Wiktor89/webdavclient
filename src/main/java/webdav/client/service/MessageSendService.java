package webdav.client.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * Сервис отправляет сообщение в очередь.
 */
public interface MessageSendService {

  /**
   * Отправка сообщения.
   * @param file payload
   * @param o
   */
  String send(MultipartFile file, String o);
}
