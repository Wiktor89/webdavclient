package webdav.client.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import webdav.client.config.WebDavServerConfig;
import webdav.client.model.ConsumerDetails;
import webdav.client.utils.HttpClientHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static webdav.client.utils.FileHeadersCustom.SEPARATOR;

@Service
@Slf4j
@RequiredArgsConstructor
public class MessageSendServiceImpl implements MessageSendService {

  private final WebDavServerConfig webDavServerCon;

  private final HttpClientHelper httpClientHelper;

  @Override
  public String send(MultipartFile multipartFile, String o) {
    ObjectMapper mapper = new ObjectMapper();
    ConsumerDetails entity = null;
    try {
      entity = mapper.readValue(o, ConsumerDetails.class);
    } catch (IOException e) {
      log.error("Convert string to object {} {} ", o, entity);
    }
    final String remoteDir = "/";
    log.debug("Create dir {} ", remoteDir);
    httpClientHelper.createDir(remoteDir);
    return unloadFile(remoteDir, multipartFile, entity);
  }

  private String unloadFile(String rootDir, MultipartFile file, ConsumerDetails val) {
    log.debug("createDir HOST USER {} ", webDavServerCon.getLogin());
    String loginAndPassword = webDavServerCon.getDomain() + "/" +  webDavServerCon.getLogin() + ":" + webDavServerCon.getPassword();
    log.debug("login : password {} ", loginAndPassword);
    List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
    interceptors.add(new LoggingRequestInterceptor());
    RestTemplate restTemplate = new RestTemplate();
    restTemplate.setInterceptors(interceptors);
    UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(webDavServerCon.getUrl() + webDavServerCon.getHostRootDir() + SEPARATOR + rootDir + SEPARATOR + file.getOriginalFilename());
    final HttpEntity entity = httpEntity(loginAndPassword, file);
    log.debug("URL download file to {} ", builder.toUriString());
    ResponseEntity<String> exchange = restTemplate.exchange(builder.toUriString(), HttpMethod.PUT, entity, String.class);
    log.debug("Response download file to {} ", exchange);
    return builder.toUriString();
  }

  private HttpEntity httpEntity(String loginAndPassword, MultipartFile file) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.MULTIPART_FORM_DATA);
    headers.set("Authorization", "NTLM " + Base64Utils.encodeToString(loginAndPassword.getBytes()));
    try {
      return new HttpEntity<>(file.getBytes(), headers);
    } catch (IOException e) {
      log.error("Content no found", e);
    }
    return null;
  }
}
