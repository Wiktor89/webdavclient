package webdav.client.exceptionHandler;

import webdav.client.exception.BadRequestException;
import webdav.client.exception.ExceptionResponse;
import webdav.client.exception.InternalServerException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.MessagingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import javax.servlet.http.HttpServletRequest;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@RestControllerAdvice
@Slf4j
public class CustomExceptionHandler {

  private final String LOG_MESSAGE = "RestControllerAdvice -> handleAccessDeniedException, correlationId = {}";
  private final String ERROR_MESSAGE = "Error: ";

  @ExceptionHandler(InternalServerException.class)
  public final ResponseEntity<ExceptionResponse> handleInternalErrorException(InternalServerException ex, HttpServletRequest request) {
    log.error(LOG_MESSAGE, ex);
    final HttpStatus status = INTERNAL_SERVER_ERROR;
    final ExceptionResponse exceptionResponse =
        ExceptionResponse.of(status.toString(), InternalServerException.class.getSimpleName(), ERROR_MESSAGE + ex.toString(), ex.toString(), request.getRequestURI());
    return new ResponseEntity(exceptionResponse, status);
  }

  @ExceptionHandler(BadRequestException.class)
  public final ResponseEntity<ExceptionResponse> handleBadRequestException(BadRequestException ex, HttpServletRequest request) {
    log.error(LOG_MESSAGE, ex);
    final HttpStatus status = HttpStatus.BAD_REQUEST;
    final ExceptionResponse exceptionResponse = ExceptionResponse.of(status.toString(), BadRequestException.class.getSimpleName(), ERROR_MESSAGE + ex.toString(), ex.toString(), request.getRequestURI());
    return new ResponseEntity(exceptionResponse, status);
  }

  @ExceptionHandler(MessagingException.class)
  public final ResponseEntity<ExceptionResponse> handleBadRequestException(MessagingException ex, HttpServletRequest request) {
    log.error(LOG_MESSAGE, ex);
    final HttpStatus status = HttpStatus.BAD_REQUEST;
    final ExceptionResponse exceptionResponse = ExceptionResponse.of(status.toString(), MessagingException.class.getSimpleName(), ERROR_MESSAGE + ex.toString(), ex.toString(), request.getRequestURI());
    return new ResponseEntity(exceptionResponse, status);
  }
}
