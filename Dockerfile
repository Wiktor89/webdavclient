FROM alpine
MAINTAINER Afonin Viktor <afonin.vitia2000@yandex.ru>

COPY target/webdavclient-*.jar /srv/service.jar

ENV CONFIG_SECRET = password \
    LOG_LEVEL = info \
    SHOW_SQL = false \
    JAVA_OPTS = "-server -Xms32m -Xmx128M -XX:+UseG1GC -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/tmp/heap_log.hprof -Djava.security.egd=file:/dev/./urandom -Djavax.net.ssl.trustStore=/srv/ca.jks"

EXPOSE 8080
WORKDIR /srv
HEALTHCHECK --start-period=90s --interval=60s --timeout=15s CMD /usr/bin/curl -sS --fail http://localhost:8080/health || exit 1
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -jar /srv/service.jar" ]